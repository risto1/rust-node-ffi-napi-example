const path = require('path')
var ffi = require('ffi-napi')

var lib = ffi.Library(path.join(__dirname, './target/debug/libcursivedylib'), {
  fibonacci: ['int', ['int']]
})

var num = lib.fibonacci(20)
console.log({ num })
